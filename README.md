# PhD thesis template for high energy physics

Contents:  
1. [Introduction](#introduction)
2. [Copying this repo](#copying)
3. [Compiling the template](#compiling)
4. [Info about the original CTAN example](#andys)

## 1) Introduction<a name="introduction"></a>
Here you will find a PhD thesis templates based on the `hepthesis` class for LaTeX. The `hepthesis` class was written by Andy Buckley to assist in writing PhD or master theses in High Energy Physics (HEP). Full documentation can be found [here](https://ctan.org/tex-archive/macros/latex/contrib/hepthesis?lang=en "CTAN.org"). Andy also wrote a good guide to avoiding common pitfalls in HEP PhD theses which you can read [here](https://zenodo.org/record/3228336). Thanks Andy!


## 2) Copying this repo<a name="copying"></a>
You may have your own way to clone/copy/fork this repo, fine. But if you want to copy the entire repo to another git host site where you can work on your thesis in private, then the following might be usefull to you:

*Note: you will not be able to pull any updates made to [the original repo](https://gitlab.com/neilwarrack/hep-phd-thesis) after doing this).*

To copy this repo (along with its history) to your preferred host site (like gitlab.cern.ch, github.com, etc., etc.) you can execute the following commands in a terminal: 

```bash
git clone --bare https://gitlab.com/neilwarrack/hep-phd-thesis.git
cd hep-phd-thesis.git
git push --mirror https://yourgitsite.com/yourusername/new-repository-name.git
```
Check it worked by visiting your new repository (which would be at https://yourgitsite.com/yourusername/new-repository-name in this case). Once you are happy that it has been copied to yourgitsite.com then you can delete the temporary bare clone of this `hep-phd-thesis`. Once that's all cleaned up you can go ahead and clone your new repo as you would normally:
```bash
cd ../
rm -rf hep-phd-thesis.git # removes the bare clone
git clone https://yourgitsite.com/yourusername/new-repository-name.git # or similar command to clone your personal copy
```
You can now push/pull and treat it as your own. Please take note: `git push --mirror` will write (or OVERWRITE!) everything from the bare clone repo to your "new-repositiry-name" repo. Take care!


## 3) Compiling the template<a name="compiling"></a>
To compile, execute the following on the command line:
```bash
# NB: This example uses pdflatex on linux (tested with Ubuntu 18.04.5 LTS in April 2021)
export TEXINPUTS=./styles:$TEXINPUTS # This tells pdflatex where to look for added style files.
pdflatex thesis.tex # this creates the .aux file needed to create the .bib file.
bibtex thesis.aux   # this creates the .bib file needed to create the bibliography.
pdflatex thesis.tex # this creates the document with the bibliography.
pdflatex thesis.tex # this creates the correct reference numbers.
```
_That's it! You should now have a newly compiled pdf file called thesis.pdf!_


## 4) Info about the original CTAN example<a name="andys"></a>

_*UPDATE*: Andy's original template now [exists on Overleaf](https://www.overleaf.com/latex/templates/hepthesis-a-template-for-academic-reports-and-phd-theses/wqrzrsppqnmb)._


To compile the example.tex file found in the example directory on the [hepthesis class webpage](https://ctan.org/tex-archive/macros/latex/contrib/hepthesis?lang=en "CTAN.org") you need the extra style files (`abhepexpt.sty`, `abhep.sty` and `abmath.sty`) which can be found here, in the `styles` directory of this repository. I presume these were also written by Andy Buckley who authored the `hepthesis` class, but they are missing from the CTAN webpage for some reason... 

You can now download and unzip the `hepthesis` directory from the CTAN hepthesis page linked above and copy the `styles` directory from this repo into the `examples` directory. Lastly, prior to compiling, you must let your latex compiler know where these extra style files are by adding the styles directory to your path using something like: `export TEXINPUTS=./styles:$TEXINPUTS`

Your latex compiler may already have the `hepthesis` class. To  find out if it does try:

```bash
find / -name 'hepthesis.cls' 2>&1 | grep -v "Permission denied"
```

You may notice that the original example suggest that you use the Inspire bib compiler but the link is broken. As a work-around you can just get references from Google scholar and copy and paste the bibtex code directly into a bib file and compile it using `bibtex yourbibfile.bib` (remember to then recompile your main .tex file again! Twice!)
